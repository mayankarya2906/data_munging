## Project Overview

In Data Munging project we have to use SOLID pinciples.
Here we have 2 ".dat" files one is weather.dat and other is football.dat.
Both these files have data which have some sort of impurities.
So,firstly we have clean the data and concern to business logic.
We just need to calculate the difference of two given features or columns correspond to given target feature.
finally print the target feature value correspond to minimum difference of given features.

## Directory Structure

 ipl_project_with_sql
    ├── README.md
    ├── requirements.txt
    ├── data_munging
    │   ├── extraction_and_finding_minimum_difference.py
    │   ├── football_diff_for_goals_and_against_goals.py
    │   ├── weather_diff_temp.py
    ├── data
        ├── football.dat
        ├── weather.dat

## Instructions
-To run this make sure you have to import re (regular expressions)
-Data must be same working directory while running code.
