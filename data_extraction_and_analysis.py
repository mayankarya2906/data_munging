import re


class DataExtractor:
    '''class DataExtractor have all the methods to extract the data'''
    def __init__(self):
        '''initialize various data structures and variables'''
        self.required_data = ""
        self.column_names = ""

    def extraction_of_data(self, file_name):
        '''return data in the form of string
           return type string'''
        required_data_temp = open(file_name, "r")
        self.required_data = \
            [each_line for each_line in required_data_temp.readlines()]
        return self.required_data

    def extract_column_names(self, required_data):
        '''extract column or features name in the form of list
            return type list'''
        for row in self.required_data:
            self.columns_names = row.split()
            return self.columns_names

    def extract_data_inside_column(self, required_data, column_names):
        '''return list of all the data inside each feature
           return type list'''

        first_iteration = True
        extracted_data = []
        
        for row in self.required_data:
            data = re.sub("[-*!@#$%^&*+/]", " ", row)
            no_of_elements_in_row = len(data.split())
            no_of_features = len(column_names)
            if first_iteration:
                extracted_data.append(data.split())
                first_iteration = False
            elif no_of_elements_in_row > no_of_features:
                extracted_data.append(data.split()[1:])
            else:
                extracted_data.append(data.split())

        column_names_from_extracted_data = extracted_data[0]
        extracted_data = extracted_data[1:]
        extracted_data.remove([])

        return extracted_data, column_names_from_extracted_data


class DataAnalyser:
    '''class DataAnalyser finds the minimum difference between
       given feature with respect to target feature'''
    def __init__(self):
        self.extract = DataExtractor()

    def filter_column_data(self, extracted_data, column_names, feature1, feature2, target_feature):
        '''return the data in the form of list of given features
           return type list'''
        feature1_data = []
        feature2_data = []
        target_feature_data = []

        index_of_feature1 = column_names.index(feature1)
        index_of_feature2 = column_names.index(feature2)
        index_of_target_feature = column_names.index(target_feature)

        for row in extracted_data:
            feature1_data.append(float(row[index_of_feature1]))
            feature2_data.append(float(row[index_of_feature2]))
            target_feature_data.append(row[index_of_target_feature])

        return feature1_data, feature2_data, target_feature_data

    def finding_diiference_minimum(self, feature1_data, feature2_data, target_feature):
        '''return type <dict> key as target feature and value as difference'''
        count = len(feature1_data)
        minimimum_diiference = {}
        for each_count in range(count):
            minimimum_diiference[target_feature[each_count]] = \
                abs(feature1_data[each_count] - feature2_data[each_count])

        low_to_high_diff = sorted(minimimum_diiference.items(), key=lambda x: x[1])
        element_with_minimum_diiference = low_to_high_diff[0]

        return element_with_minimum_diiference
