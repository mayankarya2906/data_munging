from data_extraction_and_analysis import DataAnalyser


class FootballAnalysis(DataAnalyser):
    '''class FootballAnalysis having method to find
       minimum difference between for_goals and against_goals'''

    def team_name_with_mini_diff(self):
        '''returns the tuple with the team name correspond
           to difference of for_goals and against_goals'''

        football_match = self.extract.extraction_of_data("data/football.dat")
        column_names = self.extract.extract_column_names(football_match)

        match_details, column_names = \
            self.extract.extract_data_inside_column(football_match, column_names)

        for_goals, against_goals, teams = \
            self.filter_column_data(match_details, column_names, "F", "A", "Team")

        team_name_with_mini_diff = \
            self.finding_diiference_minimum(for_goals, against_goals, teams)

        return team_name_with_mini_diff


football_analysis = FootballAnalysis()
team_with_mini_diff_goals = football_analysis.team_name_with_mini_diff()

print(team_with_mini_diff_goals)
