from data_extraction_and_analysis import DataAnalyser


class WeatherAnalyser(DataAnalyser):
    '''class WeatherAnalyser have method to find day number
       with minimum temperature spread '''

    def day_of_minimum_temp_spread(self):
        '''returns the tuple with the day number whose
           temperature spread is minimum'''

        weather = self.extract.extraction_of_data("data/weather.dat")
        column_names = self.extract.extract_column_names(weather)

        weather_details, column_names = \
            self.extract.extract_data_inside_column(weather, column_names)

        min_temp, max_temp, day_number = \
            self.filter_column_data(weather_details, column_names, "MnT", "MxT", "Dy")

        day_of_minimum_temp_spread = \
            self.finding_diiference_minimum(min_temp, max_temp, day_number)

        return day_of_minimum_temp_spread


weather_analyser = WeatherAnalyser()
minimum_temp_spread_day = weather_analyser.day_of_minimum_temp_spread()
print(minimum_temp_spread_day)
